<?php

$words = explode("\n", file_get_contents("dictionary.txt"));

$alpha = array();
foreach($words as $word) {
  $alpha[substr($word, -1, 1)][] = $word;
}

foreach($words as $word) {
  $subset = $alpha[substr($word, 0, 1)];
  foreach($subset as $dupe) {
    $combo = $word . $dupe;
    if($combo == strrev($combo)) {
      echo $word . " | " . $dupe . "\n";
    }
  }
}

